Transform = require './transform'
canvasContainer = require './canvas-container'
draw = require './draw'
getTextSize = require './get-text-size'

canvas = null
ctx = null

initByCanvasId = (canvasId) ->
  canvas = document.getElementById 'canvas'
  unless canvas?
    canvas = document.createElement 'canvas'
    document.body.appendChild canvas

setWidth = (width) -> canvas.width = width

getWidth = -> canvas.width

setHeight = (height) -> canvas.height = height

getHeight = -> canvas.height

init = (newCanvas, width = null, height = null) ->
  if (typeof newCanvas) is 'string' then initByCanvasId canvas
  else canvas = newCanvas
  ctx = canvas.getContext '2d'
  ctx.textBaseline = 'top'
  setWidth width if width?
  setHeight height if height?
  canvasContainer.canvas = canvas
  canvasContainer.ctx = ctx

applyTransform = (transform) ->
  {pos, rot, scale} = transform
  ctx.translate pos.x, pos.y
  ctx.rotate rot
  ctx.scale scale.x, scale.y

reverseTransform = (transform) ->
  inverseTransform = transform.getInverse()
  applyTransform inverseTransform

getCenterX = -> 0.5 * canvas.width

getCenterY = -> 0.5 * canvas.height

getCenter = ->
  x: getCenterX()
  y: getCenterY()

clear = -> ctx.clearRect 0, 0, canvas.width, canvas.height

getCanvas = -> canvas

getCtx = -> ctx

setAlpha = (alpha) -> ctx.globalAlpha = alpha

getAlpha = -> ctx.globalAlpha

module.exports = {Transform, draw, getTextSize, init, getCanvas, getCtx,
  setWidth, getWidth, setHeight, getHeight, getCenterX, getCenterY, getCenter,
  clear, setAlpha, getAlpha, applyTransform, reverseTransform}
