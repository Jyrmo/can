circle = require './draw/circle'
rect = require './draw/rect'
sprite = require './draw/sprite'
text = require './draw/text'

module.exports = {circle, rect, sprite, text}
