getDefaultPos = ->
  x: 0
  y: 0

getDefaultScale = ->
  x: 1
  y: 1

class Transform
  constructor: (@pos = getDefaultPos(), @rot = 0, @scale = getDefaultScale()) ->

  getInverse: ->
    pos =
      x: -@pos.x
      y: -@pos.y
    rot = -@rot
    scale =
      x: 1 / @scale.x
      y: 1 / @scale.y
    new Transform pos, rot, scale

  translate: (vector) ->
    @pos.x += vector.x
    @pos.y += vector.y

module.exports = Transform
