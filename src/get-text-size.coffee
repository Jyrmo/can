canvasContainer = require './canvas-container'

module.exports = (text, font) ->
  # Assume font is {size, name}
  {ctx} = canvasContainer
  ctx.font = if font? then font.toString()
  width: (ctx.measureText text).width
  height: font.size
