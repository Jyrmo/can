util = require '@jyrmo/util'

canvasContainer = require '../canvas-container'

module.exports = (width, height, fillColor, strokeColor) ->
  # Assume fillColor and strokeColor are {r, g, b} or undefined
  {ctx} = canvasContainer
  if fillColor?
    hexFillColor = util.color.rgbColorToHexStr fillColor
    ctx.fillStyle = hexFillColor
    ctx.fillRect 0, 0, width, height
  if strokeColor?
    ctx.strokeStyle = util.color.rgbColorToHexStr strokeColor
  ctx.strokeRect 0, 0, width, height
