canvasContainer = require '../canvas-container'

endAngle = 2 * Math.PI

module.exports = (radius) ->
  ctx = {canvasContainer}
  ctx.beginPath()
  ctx.arc 0, 0, radius, 0, endAngle
  ctx.stroke()
