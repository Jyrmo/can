canvasContainer = require '../canvas-container'

module.exports = (address) ->
  {imgId, x, y, width, height} = address
  img = document.getElementById imgId
  {ctx} = canvasContainer
  ctx.drawImage img, x, y, width, height, 0, 0, width, height
