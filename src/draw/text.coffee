util = require '@jyrmo/util'

canvasContainer = require '../canvas-container'

module.exports = (text, font, color) ->
  # Assume color is {r, g, b}
  {ctx} = canvasContainer
  if font? then ctx.font = font
  if color?
    hexColor = util.color.rgbColorToHexStr color
    ctx.fillStyle = hexColor
  ctx.fillText text, 0, 0
